import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { TutComponent } from './tut/tut.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { ConfigService } from './tut/shared/config.service';


@NgModule({
  declarations: [
    AppComponent,
    TutComponent,
    TutorialComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
