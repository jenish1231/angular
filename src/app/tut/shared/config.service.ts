import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Employee} from './Employee.model';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers : new HttpHeaders({
    'Content-Type':'application/json',
    'Authorization':'my-auth-token'
  })
};

@Injectable()
export class ConfigService {
  
  constructor(private http : HttpClient) { }

  getData() : Observable<Employee> {
    return this.http.get<Employee>
    ("http://dummy.restapiexample.com/api/v1/employee");
  }

  //requesting a non-json data
  getTextFile(filename : string) {
    return this.http.get(filename, {responseType:'text'});
  }

  addData(employee : Employee):Observable<Employee> {
    return this.http.post<Employee>("http://dummy.restapiexample.com/api/v1/create",employee,httpOptions);
  }

  deleteData(id: number) :Observable<{}> {
    return this.http.delete("http://dummy.restapiexample.com/api/v1/update/"+id,httpOptions);
  }

  putData(employee : Employee):Observable<Employee> {
    return this.http.put<Employee>("http://dummy.restapiexample.com/api/v1/update/21",employee,httpOptions);
  }

  private handleError (error : HttpErrorResponse) {
    if(error.error instanceof ErrorEvent) {
      console.error('An error occured : ' , error.error.message);
    }else {
      console.error(
        `BackEnd returned code ${error.status},`+ 
        `body was : ${error.error}`
      );
    }

   // return throwError('Something bad happened');
  }

}
