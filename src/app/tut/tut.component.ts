import { Component, OnInit, Output } from '@angular/core';
import { Hero } from './shared/hero';
import { NgForm, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import {ConfigService} from './shared/config.service';
import { Employee } from './shared/Employee.model';

@Component({
  selector: 'app-tut',
  templateUrl: './tut.component.html',
  styleUrls: ['./tut.component.css']
})
export class TutComponent implements OnInit {

  powers = ['Really Smart','Super Flexible','Super Hot','Weather Changer'];
  model = new Hero(1,'Dr IQ',this.powers[0],'Chuck Overstreet');

  submitted = false;
  visible = true;
  employee : Employee;
  error : string;
  ngOnInit() {
    //this.heroFrom = new FormGroup();
  }

  constructor(private configService : ConfigService) {

  }

  onSubmit(form:NgForm) {
    this.submitted = true;
    
  }

  get diagostic() {
    return JSON.stringify(this.model);
  }

  newHero(){
    this.model = new Hero(42,'','');
  }

  showConfig() {
    this.configService.getData()
    .subscribe();
  }

  addData() {
    this.employee = {
      id : 1,
      employee_name:"Jenish",
      employee_salary:22000,
      employee_age : 22,
      profile_image: "www.asdfasdf"
    };

    this.configService.addData(this.employee).subscribe();
  }

  deleteData() {
    this.configService.deleteData(6).subscribe();
  }
  

  

}
